# README - Student Network #

Student Network (Studentska drustvena mreza, cro.) is a small social networking web application made for a college assignment (Web Application Programming, prof. Alen Šimec, Zagreb Polytechnic).

Apps design and functionality is based on Twitter, one of the bigest social networks on the web.

Primary objective of the assignment was to use and demonstrate the latest version of the PHP language, version 7.

The app also uses "Sohn lab Social Auth v2", a third party plugin for user authorization via existing web services such as Facebook, Twitter and Google+.